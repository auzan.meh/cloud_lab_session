import csv
import json
import uuid
from datetime import datetime

import pandas

data = {}
result = ""
with open('all_stocks_5yr.csv') as csvFile:
    df = pandas.read_csv(csvFile)
    for i in range(len(df)):
        name = df.loc[i, "Name"]
        str_Date =str(df.loc[i, "date"])
        dt_obj = datetime.strptime(str_Date,'%Y-%m-%d').date()
        close = df.loc[i, "close"]
        if name not in data:
            data[name]= {}
        data[name][str_Date]=close

# Serializing json
json_object = json.dumps(data, indent=4)

# Writing to sample.json
with open("../database/sample.json", "w") as outfile:
    outfile.write(json_object)