import os
from flask import Flask, make_response
import json
from flask import request, redirect
from datetime import date, timedelta
from database.database_investment import USER_CREDENTIALS, USER_HOLDINGS, CURRENT_MARKET_PRICES
from database.database_manager import *
from flask_cors import CORS


app = Flask(__name__)

CORS(app)


@app.route('/')
def main_page():
    return f"Welcome to the Portfolio Dashboard!"


@app.route("/login", methods=['POST'])
def login():
    received_credentials = request.get_json()
    user_cred_result = check_user_credentials(received_credentials['username'], received_credentials['password'])
    if user_cred_result == "User not found.":
        return json.dumps({"success": False, "message": f"User not found - {received_credentials['username']}"})
    resp = make_response(redirect('/account'))
    resp.set_cookie('user_id', user_cred_result)
    return resp


@app.route("/account", methods=['GET'])
def account_summary():
    try:
        user_id = request.cookies.get('user_id')
        summary = {'portfolio_value': _calculate_portfolio_value(user_id),
                   'holdings': _get_portfolio(user_id),
                   'daily_pnl': _get_daily_pnl(user_id)}
        return summary
    except KeyError as error:
        return json.dumps({"success": False, "message": f"Key was not found - {error}"})


@app.route("/create_account", methods=['POST'])
def create_account():
    received_credentials = request.get_json()
    user_id = add_user_credentials(received_credentials['username'], received_credentials['password'])
    add_user_holdings(user_id, random_portfolio_generator_function())
    return json.dumps({"success": False, "message": "User was successfully added."})


def get_market_data():  # marketdata
    pass


def _get_portfolio(user_id, date=None):
    try:
        holdings = USER_HOLDINGS[user_id]
        portfolio_dict = {}
        for s in holdings:
            if date:
                price = LATEST_DATA[s][date]
            else:
                price = CURRENT_MARKET_PRICES[s]
            portfolio_dict[s] = {'qty': holdings[s], 'price': price}
        return portfolio_dict
    except Exception as error:
        raise Exception(f"Error during get_portfolio()- {error}")


def _calculate_portfolio_value(user_id, date=None):
    try:
        # Date is only present when the non current portfolio value is desired
        if date:
            portfolio = _get_portfolio(user_id, date)
        else:
            portfolio = _get_portfolio(user_id)
        portfolio_values = {}
        for s in portfolio:
            value = portfolio[s]['qty'] * portfolio[s]['price']
            portfolio_values[s] = value
        portfolio_value = sum(portfolio_values.values())
        return portfolio_value
    except Exception as error:
        raise Exception(f"Error during _calculate_portfolio_value()- {error}")


@app.route("/daily_pnl", methods=['POST'])
def _get_daily_pnl(user_id):
    current_value = _calculate_portfolio_value(user_id)
    old_Value = _calculate_portfolio_value(user_id, str(date.today() - timedelta(2)))
    return f"{str((current_value - old_Value) / current_value)}%"


@app.route('/current_market_price', methods=['POST'])
def current_market_price():  #
    data = request.get_json()
    return get_latest_stock_value(data['ticker'])


def _get_weekly_pnl(user_id):
    current_value = _calculate_portfolio_value(user_id)
    old_Value = _calculate_portfolio_value(user_id, str(date.today - timedelta(2)))
    return current_value - old_Value


def _get_monthly_pnl(user_id):
    current_value = _calculate_portfolio_value(user_id)
    old_Value = _calculate_portfolio_value(user_id, date.today - timedelta(1))
    return current_value - old_Value


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get("PORT", 8080)))
