import datetime
import yfinance as yf
from database.database_investment import *
import uuid
from database.latest_data import LATEST_DATA


def add_user_credentials(user_name, password):
    user_id = str(uuid.uuid4())
    password_hash = str(hash(password))
    USER_CREDENTIALS[user_id] = {
        'username': f"{user_name}",
        'password_hash': f"{password_hash}"
    }
    return user_id


def check_user_credentials(user_name, password):
    password_hash = str(hash(password))
    for each_user_id in USER_CREDENTIALS:
        if user_name == USER_CREDENTIALS[each_user_id]['username'] and password_hash == USER_CREDENTIALS[each_user_id][
            'password_hash']:
            return each_user_id
    return f"User not found."


def return_latest_stock_data(ticker):
    latest_data = {}
    for each_ticker in LATEST_DATA:
        latest_date = list(LATEST_DATA[each_ticker])[-1]
        latest_data[each_ticker] = latest_date
    return latest_data


def add_user_holdings(user_id, holdings):
    if user_id not in USER_HOLDINGS:
        USER_HOLDINGS[user_id] = holdings


def get_latest_stock_value(stockTicker):
    ticker = yf.Ticker(stockTicker)
    price = str(ticker.info['regularMarketPrice'])
    return price,
