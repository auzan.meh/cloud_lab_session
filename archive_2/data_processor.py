import json
from datetime import datetime

import pandas

data = {}
result = ""
with open('sp500_stocks.csv') as csvFile:
    df = pandas.read_csv(csvFile)
    for i in range(len(df)):
        name = df.loc[i, "Symbol"]
        str_Date =str(df.loc[i, "Date"])
        dt_obj = datetime.strptime(str_Date,'%Y-%m-%d').date()
        close = df.loc[i, "Close"]
        if name not in data:
            data[name]= {}
        if dt_obj.year == 2023:
            data[name][str_Date]=close

# Serializing json
json_object = json.dumps(data, indent=4)

# Writing to sample.json
with open("../database/latest_stocks_data_2023.json", "w") as outfile:
    outfile.write(json_object)