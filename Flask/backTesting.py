import math
import numpy as np
import RestService
from historicalSimulation import historical_calculation
from modelBuilding import model_building_calculation
from monteCarlo import monte_carlo_calculation

def get_exceedances(returns, estimated_var, day_count):
    '''
    Returns the integer number of times the estimated VaR for a portfolio is exceeded.

        Parameters:
            returns (DataFrame): The portfolio of assets used to test the VaR computation
            estimated_var (float): The VaR computed using the program's computation approaches for the given portfolio
            day_count (int): The number of days the portfolio covers
    
        Returns:
            exceedances (int): Number of times the estimated VaR was exceeded in reality
    '''
    exceedances = 0

    for i in range(day_count - 1):
        if not (math.isnan(returns['% change'][i])):
            if (- estimated_var > returns['% change'][i]):
                exceedances += 1

    print('TEST EXCEEDANCES', exceedances)
    
    return exceedances

def back_testing(method, tickers, weights, start_date, end_date, init_investment):
    '''
    Returns the integer number of exceedances and whether to accept the accuracy of a VaR computation approach.

        Parameters:
            method (str): The VaR computation methodology to be tested
            tickers (Array[str]): The tickers associated with the portfolio's assets
            start_date (str): The day the time horizon starts from
            end_date (str): The day the time horizon ends on
            init_investment (int): The initial investment level associated with the portfolio

        Returns:
            exceedances (int): Number of times the estimated VaR was exceeded in reality
    '''
    
    dates = [start_date, end_date]
    returns = RestService.fetch_data(tickers, dates)

    if method == "modelBuilding":
        computed_var, conditional_var = model_building_calculation(tickers, start_date, end_date, init_investment, weights)
    elif method == "historical":
        computed_var, conditional_var = historical_calculation(tickers, weights, start_date, end_date, init_investment)
    else:
        computed_var, conditional_var = monte_carlo_calculation(tickers, start_date, end_date, init_investment, weights)
   
    estimated_var = computed_var / init_investment

    print('computed', computed_var)
    print('estimated', estimated_var)  

    if (len(tickers)) > 1:
        returns['Portfolio'] = (weights * returns.values).sum(axis=1)
        returns['% change'] = returns['Portfolio'].pct_change().dropna()
    else:
        returns['% change'] = returns.pct_change().dropna()

    print(returns['% change'])

    day_count = len(returns['% change'])
    exceedances = get_exceedances(returns, estimated_var, day_count)

    fifth_percentile_days = (5 * day_count) / 100.0

    if (exceedances > fifth_percentile_days):
        return exceedances, "reject"
    else:
        return exceedances, "accept"

