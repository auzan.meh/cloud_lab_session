import RestService

#Standard Library Imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Title: VaR & CVaR with Monte Carlo Simulation
# Author: ASX Portfolio
# Date: 01/2022
# Availability: https://asxportfolio.com/shares-risk-measures-VaR4

def get_monte_carlo_var(returns,alpha=5):
    '''
    Determines the exact VaR for the 5th percent confidence interval.

        Parameters:
            returns (DataFrame): The weighted portfolio of assets having its VaR computed 
            alpha (int): The percentile of the data to compute, hard-coded to 5
        Returns:
            The specified 5th percentile of the input Profits and Losses
    '''
    return np.percentile(returns, alpha)

def get_monte_carlo_cvar(returns, alpha=5):
    '''
    Determines the conditional VaR for the 5th percent confidence interval.

        Parameters:
            returns (DataFrame): The weighted portfolio of assets having its VaR computed 
            alpha (int): The percentile of the data to compute, hard-coded to 5
        Returns:
            The average of the values which exceeded the estimated VaR
    '''
    below_var = returns <= get_monte_carlo_var(returns, alpha=alpha)
    return returns[below_var].mean()


def monte_carlo_calculation(tickers, start_date, end_date, init_investment, weights):
    '''
    Determines the Monte Carlo Simulation VaR with respect to the user's input and initial investment.

        Parameters:
            tickers (Array[str]): The tickers corresponding to each asset of the portfolio
            start_date (str): The day the time horizon starts from
            end_date (str): The day the time horizon ends on
            init_investment (int): The initial investment level associated with the portfolio
            weights (Array[float]): The weights associated with each asset of the portfolio

        Returns:
            The VaR and CVaR on the initial investment
    '''
    dates = [start_date, end_date]
    stock_list = tickers

    is_multi_asset = (len(weights) > 1)

    #if single-asset
    if not (is_multi_asset):
        weights = np.array([1])

    weights = np.array(weights)

    try:
        port_results = get_portfolio_simulations(stock_list, dates, weights, init_investment)
        
        monte_carlo_var = init_investment - get_monte_carlo_var(port_results, alpha=5)
        conditional_var = init_investment - get_monte_carlo_cvar(port_results, alpha=5)


    except:
        default_pnl, stocks_default = RestService.get_default_values(is_multi_asset, dates)
        
        port_results = get_portfolio_simulations(stocks_default, dates, weights, init_investment)
        
        monte_carlo_var = init_investment - get_monte_carlo_var(port_results, alpha=5)
        conditional_var = init_investment - get_monte_carlo_cvar(port_results, alpha=5)

    print('The Monte Carlo Simulation VaR is ${}'.format(round(monte_carlo_var,2)))
    print('The Conditional Monte Carlo Simulation VaR is ${}'.format(round(conditional_var,2)))

    return round(monte_carlo_var,2), round(conditional_var,2)

def plot_graph(simulations):
    '''
    Creates graph displaying all of the associated simulations for a given portfolio with portfolio value against days.
    '''
    plt.plot(simulations)
    plt.ylabel('Portfolio Value ($)')
    plt.xlabel('Days')
    plt.title('Monte Carlo Simulation')
    #plt.show()


def get_portfolio_simulations(stock_list, dates, weights, init_investment):
    '''
    Applies the weight associated with each asset of the portfolio.

        Parameters:
            stock_list (Array[str]): The string tickers associated with each asset
            dates (Array[str]): The start and end dates specifying the time horizon
            weights (Array[float]): The weights corresponding to each asset
            init_investment (int): The initial investment level associated with the portfolio
    
        Returns:
            port_results (Series): The portfolio after having the specified number of simulations performed on it
    '''
    #number of simulations
    NO_OF_SIMS = 10

    #timeframe in days
    TIMEFRAME = 100 

    returns = RestService.fetch_data(stock_list, dates)

    returns = returns.pct_change()
    cov_matrix, meanReturns = RestService.get_cov_matrix(returns)


    portfolio_simulations = perform_simulations(meanReturns, cov_matrix, NO_OF_SIMS, TIMEFRAME, float(init_investment), weights)
    
    # plot_graph(portfolio_simulations)

    port_results = pd.Series(portfolio_simulations[-1,:])

    return port_results

def perform_simulations(meanReturns, cov_matrix, no_of_sims, timeframe, initial_portfolio, weights):
    '''
    Performs the specified number of simulations on the averaged returns using Cholesky decomposition.

        Parameters:
            meanReturns (DataFrame): The average returns for the specified portfolio
            cov_matrix (Array): The covariance matrix computed on the returns using its weights
            NO_OF_SIMS (int): The number of simulations to perform
            timeframe (int): The timeframe to perform the simulations over in days
            initial_portfolio (int): The initial investment level associated with the portfolio
            weights (Array[float]): The weights corresponding to each asset

        Returns:
            portfolio_sims (DataFrame): The cumulative returns after all simulations have been performed
    '''

    #mean returns
    meanM = np.full(shape=(timeframe, len(weights)), fill_value=meanReturns)
    
    #transpose of matrix
    meanM = meanM.T

    #stores all of the information from simulations
    portfolio_sims = np.full(shape=(timeframe, no_of_sims),fill_value=0.0)


    for m in range(0, no_of_sims):
        #assuming daily returns are distributed by a multivariate normal distribution

        #Z = samples from a normal distribution
        Z = np.random.normal(size=(timeframe, len(weights)))

        #L = cholesky matrix to determine lower triangular matrix
        L = np.linalg.cholesky(cov_matrix)

        daily_returns = meanM + np.inner(L, Z)

        #records cumulative returns
        portfolio_sims[:,m] = np.cumprod(np.inner(weights,daily_returns.T)+1)*initial_portfolio

    return portfolio_sims



