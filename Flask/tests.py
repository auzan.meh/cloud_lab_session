from historicalSimulation import historical_calculation
from modelBuilding import model_building_calculation
from monteCarlo import monte_carlo_calculation
from backTesting import back_testing
from monteCarloTestData import return_single_asset_mocked_z, return_multi_asset_mocked_z

import random
from flask import Flask
import unittest
from unittest import mock
from flask_testing import TestCase

from unittest import TestCase, mock

class TestVaRCalculation(TestCase):

    ''' Used to test the various VaR computation methodologies for single- and multi-asset cases.'''

    def create_app(self):

        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config['SECRET_KEY'] = 'c5669f1d005405facd9367a26b62071b'
        return app
        
    def test_singleAssetHistorical(self):
        ticker = ['GME']
        start_date = '2017-02-01'
        end_date = '2017-03-16'
        weights = []
        init_investment = 10000

        computed_var, conditional_var = historical_calculation(ticker, weights, start_date, end_date, init_investment)
        self.assertEqual(336.2, computed_var)
        self.assertEqual(588.74, conditional_var)

    def test_multiAssetHistorical(self):
        ticker1 = 'GE'
        ticker2 = 'GOOG'
        ticker3 = 'AAPL'
        start_date = '2017-02-01'
        end_date = '2017-03-16'

        tickers = [ticker1,ticker2,ticker3]
        weights = [0.5,0.2,0.3]

        init_investment = 10000

        computed_var, conditional_var = historical_calculation(tickers, weights, start_date, end_date, init_investment)
        self.assertEqual(47.62, computed_var)
        self.assertEqual(60.27, conditional_var)

    # def test_invalidTickerSingleHistorical(self):
        
    #     ticker = ['TIA']
    #     start_date = '2016-10-10'
    #     end_date = '2017-03-16'
    #     weights = []
    #     init_investment = 10000

    #     computed_var, conditional_var =  historical_calculation(ticker, weights, start_date, end_date, init_investment)
        
    #     # AAPL VaR for given dates
    #     defaultVaR = 0.009503742495060285
        
        # self.assertEqual(95.04, computed_var)
        # self.assertEqual(189.62, conditional_var)

    # def test_invalidTickerMultiHistorical(self):
    #     ticker1 = 'GOOG'
    #     ticker2 = 'TIA'
    #     ticker3 = 'AAPL'
    #     start_date = '2016-10-10'
    #     end_date = '2017-03-16'
    #     weights = [0.5,0.2,0.3]
    #     init_investment = 10000

    #     tickers = [ticker1,ticker2,ticker3]

    #     computed_var, conditional_var = historical_calculation(tickers, weights, start_date, end_date, init_investment)

    #     defaultVaR = 0.009260580162238075

    #     self.assertEqual(92.61, computed_var)
    #     self.assertEqual(134.31, conditional_var)


    def test_singleAssetModelBuilding(self):

        ticker = ['GME']
        start_date = '2017-02-01'
        end_date = '2017-03-16'
        init_investment = 100000
        weights = []

        computed_var, conditional_var = model_building_calculation(ticker, start_date, end_date, init_investment, weights)

        self.assertEqual(3488.4, computed_var)
        self.assertEqual(4391.78, conditional_var)

    def test_multiAssetModelBuilding(self):
        ticker1 = 'GE'
        ticker2 = 'GOOG'
        ticker3 = 'AAPL'
        start_date = '2017-02-01'
        end_date = '2017-03-16'
        init_investment = 100000

        tickers = [ticker1,ticker2,ticker3]
        weights = [0.5, 0.2, 0.3]

        computed_var, conditional_var = model_building_calculation(tickers, start_date, end_date, init_investment, weights)

        self.assertEqual(689.1, computed_var)
        self.assertEqual(903.48, conditional_var)
        
    # def test_invalidTickerSingleModelBuilding(self):
        
    #     ticker = ['TIA']
    #     start_date = '2016-10-10'
    #     end_date = '2017-03-16'
    #     weights = []
    #     init_investment = 100000

    #     computed_var, conditional_var = model_building_calculation(ticker, start_date, end_date, init_investment, weights)
        
    #     # AAPL VaR for given dates
    #     defaultVaR = 1461.36

    #     self.assertEqual(computed_var, defaultVaR)
    #     self.assertEqual(1881.42, conditional_var)

    # def test_invalidTickerMultiModelBuilding(self):

    #     ticker1 = 'GE'
    #     ticker2 = 'TIA'
    #     ticker3 = 'AAPL'
    #     start_date = '2016-10-10'
    #     end_date = '2017-03-16'
    #     init_investment = 100000

    #     tickers = [ticker1,ticker2,ticker3]
    #     weights = [0.5, 0.2, 0.3]

    #     computed_var, conditional_var = model_building_calculation(tickers, start_date, end_date, init_investment, weights)

    #     defaultVaR = 1051.57

    #     self.assertEqual(computed_var, defaultVaR)
    #     self.assertEqual(1341.63, conditional_var)

    @mock.patch('monteCarlo.np.random.normal')
    def test_singleAssetMonteCarlo(self, mock_choice):

        ticker = ['AAPL']
        start_date = '2017-02-01'
        end_date = '2017-03-16'
        init_investment = 10000
        weights =[]

        mock_choice.side_effect = return_single_asset_mocked_z()

        computed_var, conditional_var = monte_carlo_calculation(ticker, start_date, end_date, init_investment, weights)
    
        self.assertEqual(computed_var, -3425.96)
        self.assertEqual(conditional_var, -3363.97)


    @mock.patch('monteCarlo.np.random.normal')
    def test_arbitraryMultiAssetMonteCarlo(self, mock_choice):

        ticker1 = 'GE'
        ticker2 = 'GOOG'
        ticker3 = 'AAPL'
        ticker4 = 'GME'
        start_date = '2017-02-01'
        end_date = '2017-03-16'
        init_investment = 100000

        tickers = [ticker1,ticker2,ticker3,ticker4]
        weights = [0.5, 0.2, 0.2, 0.1]

        mock_choice.side_effect = return_multi_asset_mocked_z()

        computed_var, conditional_var = monte_carlo_calculation(tickers, start_date, end_date, init_investment, weights)

        self.assertEqual(computed_var, -24413.85)
        self.assertEqual(conditional_var, -23879.04)

    def test_back_testingHistorical(self):
        ticker = ['GME']
        start_date = '2016-11-28'
        end_date = '2017-04-21'
        weights = []
        init_investment = 10000
        method = "historical"

        exceedances, result = back_testing(method, ticker, weights, start_date, end_date, init_investment)

        self.assertEqual(exceedances, 5)
        self.assertEqual(result, "accept")  

    def test_back_testingModelBuilding(self):
        tickers = ['GME', 'GOOG']
        start_date = '2016-11-28'
        end_date = '2017-04-21'
        weights = [0.6, 0.4]
        init_investment = 10000
        method = "modelBuilding"

        exceedances, result = back_testing(method, tickers, weights, start_date, end_date, init_investment)

        self.assertEqual(exceedances, 0)
        self.assertEqual(result, "accept") 


if __name__ == '__main__':
    unittest.main()      
     