import RestService
import numpy as np
from scipy.stats import norm

# Title: Parametric VaR with Python
# Author: ASX Portfolio
# Date: 10/2021
# Availability: https://asxportfolio.com/shares-risk-measures-VaR3

def model_building_calculation(tickers, start_date, end_date, init_investment, weights):
    '''
    Determines the Model-Building VaR with respect to the user's input and initial investment.

        Parameters:
            tickers (Array[str]): The tickers corresponding to each asset of the portfolio
            start_date (str): The day the time horizon starts from
            end_date (str): The day the time horizon ends on
            init_investment (int): The initial investment level associated with the portfolio
            weights (Array[float]): The weights associated with each asset of the portfolio

        Returns:
            model_building_var (float): The Model-Building VaR on the initial investment
            conditional_var (float): The Conditional Model-Building VaR on the initial investment
    '''
    is_multi_asset = (len(weights) > 1)

    #if single-asset
    if not (is_multi_asset):
        weights = np.array([1])

    dates = [start_date, end_date]

    returns = RestService.fetch_data(tickers, dates)

    try:
        model_building_var, conditional_var = get_model_building_var(returns, weights, init_investment)
    except:
        defaultPnL, stocksDefault = RestService.get_default_values(is_multi_asset, dates)
        model_building_var, conditional_var = get_model_building_var(defaultPnL, weights, init_investment)

    print(f'The Model-Building VaR is ${model_building_var}')
    print(f'The Model-Building Conditional VaR is ${conditional_var}')

    return model_building_var, conditional_var

def get_model_building_var(returns, weights, init_investment):
    '''
    Determines the exact historical VaR for the 5th percent confidence interval by computing covariance matrix.

        Parameters:
            returns (DataFrame): The weighted portfolio of assets having its VaR computed 
            weights (Array[float]): The weights associated with each asset of the portfolio
            init_investment (int): The initial investment level associated with the portfolio
        Returns:
            model_building_var_II (float): The VaR on the initial investment
            model_building_cvar_II (float): The CVaR on the initial investment
    '''
    returns = returns.pct_change().dropna()
    returns.tail()

    cov_matrix, avg_rets = RestService.get_cov_matrix(returns)

    #1 day
    TIME = 1

    # 5th percent confidence interval
    ALPHA = 5

    p_returns, p_std = RestService.portfolio_performance(np.array(weights), avg_rets, cov_matrix, TIME)

    model_building_var = norm.ppf(1-ALPHA/100)*p_std-p_returns
    modelBuildingCVaR = cVar_model_building(p_returns, p_std)

    model_building_var_II = round(int(init_investment)*model_building_var, 2)
    model_building_cvar_II = round(int(init_investment)*modelBuildingCVaR, 2)

    return model_building_var_II, model_building_cvar_II


def cVar_model_building(portfolio_return, portfolio_std, alpha=5):
    '''
    Applies the CVaR formula on the portfolio and returns CVaR for 5th percent confidence interval using the standard deviation.
    '''
    cVaR = ((alpha/100)**-1) * norm.pdf(norm.ppf(alpha/100))*portfolio_std - portfolio_return
    return cVaR


