import numpy as np
import RestService

# Title: Historical VaR with Python
# Author: ASX Portfolio
# Date: 09/2021
# Availability: https://asxportfolio.com/shares-risk-measures-VaR2

def apply_weights(returns, stocks, weights, is_multi_asset):
    '''
    Applies the weight associated with each asset of the portfolio.

        Parameters:
            returns (DataFrame): The portfolio of assets having its VaR computed 
            stocks (Array[str]): The string tickers associated with each asset
            weights (Array[float]): The weights corresponding to each asset
            is_multi_asset (boolean): Determines whether the portfolio is single- or multi-asset
    
        Returns:
            returns (DataFrame): The portfolio with the corresponding weight of each asset applied
    '''
    #determine if single-asset or multi-asset
    if is_multi_asset:
        weights /= np.sum(weights)
        returns['portfolio'] = returns.dot(weights)
    else:
        returns['portfolio'] = returns.sort_values(by=f"WIKI/{stocks[0]} - Adj. Close")

    return returns


def get_historical_var(pnl, alpha):
    '''
    Determines the exact historical VaR for the 5th percent confidence interval.

        Parameters:
            pnl (DataFrame): The weighted portfolio of assets having its VaR computed 
            alpha (int): The percentile of the data to compute
        Returns:
            The specified 5th percentile of the input Profits and Losses
    '''
    return np.percentile(pnl, alpha)

def get_conditional_var(pnl, alpha=5):
    '''
    Determines the conditional historical VaR for the 5th percent confidence interval.

        Parameters:
            pnl (DataFrame): The weighted portfolio of assets having its VaR computed 
            alpha (int): The percentile of the data to compute
        Returns:
            The average of the values which exceeded the estimated VaR
    '''
    below_var = pnl <= get_historical_var(pnl, alpha=alpha)
    return pnl[below_var].mean()

def historical_calculation(tickers, weights, start_date, end_date, initInvestment):
    '''
    Determines the historical VaR with respect to the user's input and initial investment.

        Parameters:
            tickers (Array[str]): The tickers corresponding to each asset of the portfolio
            weights (Array[float]): The weights associated with each asset of the portfolio
            start_date (str): The day the time horizon starts from
            end_date (str): The day the time horizon ends on
            initInvestment (int): The initial investment level associated with the portfolio

        Returns:
            historic_VaR_II (float): The Historical Simulation VaR on the initial investment
            conditional_var_II (float): The Conditional Historical Simulation VaR on the initial investment
    '''
    dates = [start_date, end_date]
    
    no_stocks = len(tickers)
    is_multi_asset = (no_stocks > 1)

    #1 day
    TIME = 1

    pnl = RestService.fetch_data(tickers, dates)
    pnl = pnl.pct_change().dropna()
    
    try:
        pnl = apply_weights(pnl,tickers, weights, is_multi_asset)

        # using square root of time rule
        historic_VaR = - get_historical_var(pnl['portfolio'], alpha=5)*np.sqrt(TIME)
        conditional_var = - get_conditional_var(pnl['portfolio'], alpha=5)*np.sqrt(TIME)
    except:
        default_pnl, stocksDefault = RestService.get_default_values(is_multi_asset, dates)
        default_pnl = default_pnl.pct_change().dropna()
        default_pnl = apply_weights(default_pnl,stocksDefault, weights, is_multi_asset)
        
        #Negative to generate loss as a positive figure
        historic_VaR = - get_historical_var(default_pnl['portfolio'], alpha=5)*np.sqrt(TIME)
        conditional_var = - get_conditional_var(default_pnl['portfolio'], alpha=5)*np.sqrt(TIME)

    historic_VaR_II = round(int(initInvestment)*historic_VaR, 2)
    conditional_var_II = round(int(initInvestment)*conditional_var, 2)

    print(f'The simple historical VaR is ${historic_VaR_II}')
    print(f'The simple historical Conditional VaR is ${conditional_var_II}')

    return historic_VaR_II, conditional_var_II

