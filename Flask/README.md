# Flask Back-End for VaR computation

This consists of various modules, implementing:
* Historical Simulation
* Model Building
* Back-testing
* Monte Carlo Simulation
* Rest Service (used to 'output' and 'receive' data from Angular)

## Testing:

Unit tests can be found in the 'Tests' directory, and run using:

```bash
python tests.py
```

<br>

The data used to mock the random number generation of the Monte Carlo Simulation can be found in 'Tests/monteCarloTestData.py'

<br>

# Credits
Citation information can be found above re-used or inspired code.

