from os import environ
from flask import Flask, jsonify, request, url_for, flash, redirect
from flask_cors import CORS
import numpy as np

import quandl

import historicalSimulation
import modelBuilding
import monteCarlo

# Title: Integrate Angular 7|8|9 with Flask Python Tutorial
# Author: Codez Up
# Date: 01/2022
# Availability: https://codezup.com/integrate-angular-with-flask-python-tutorial/#comments

quandl.ApiConfig.api_key = environ.get('QUANDL_KEY')

calculated_var = 'No data to show'
conditional_var = 'No data to show'

portfolio = 'No data to show'
app = Flask(__name__)
app.config.from_pyfile('config.py')


CORS(app)

def get_cov_matrix(returns):
    '''
    Determines the covariance matrix and average returns for a weighted portfolio.

        Parameters:
            returns (DataFrame): The weighted portfolio of assets having its VaR computed 

        Returns:
            cov_matrix (Array): The covariance matrix associated with the returns
            mean_returns (DataFrame): The average of the returns corresponding to the portfolio
    '''
    mean_returns = returns.mean()
    cov_matrix = returns.cov()

    return cov_matrix, mean_returns

def portfolio_performance(weights, mean_returns, cov_matrix, time):
    '''
    Determines the standard deviation of the returns for the supplied weights and time-horizon.
    '''
    returns = np.sum(mean_returns*weights)*time
    std = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))*np.sqrt(time)

    #pRet, pStd
    return returns, std

def fetch_data(stocks, dates):
    '''
    Gets the Adjusted Close data associated with the user-specified portfolio using Quandl API.

        Parameters:
            stocks (Array[str]): The tickers associated with each asset of the portfolio
            dates (Array[int]): The start and end dates of the specified time horizon
        Returns:
            returns (DataFrame): The unweighted closing data of all assets specified in the portfolio
    '''
    
    stock_fetch = []
    for stock in stocks:
        stock_fetch.append(f"WIKI/{stock.upper()}")

    portfolio = quandl.get(stock_fetch, start_date=dates[0], end_date=dates[1])

    returns = portfolio[list(portfolio.columns[portfolio.columns.str.contains("Adj. Close")])]

    return returns

@app.route("/outputData/", methods = ['GET', 'POST'])
def output_data():
    '''
    Outputs the computed VaR and CVaR as a JSON oject on the relevant route to be fetched by Angular.
    '''
    global calculated_var
    global conditional_var
    global portfolio

    testData = {
  "holdings": {
    "FTSE": {
      "price": 5000,
      "qty": 5
    },
    "USD": {
      "price": 1,
      "qty": 1
    }
  },
  "portfolio_value": 25001
}

    return jsonify(testData)
    # return jsonify([calculated_var, conditional_var])

@app.route("/inputData/", methods = ['GET', 'POST'])
def input_data():
    '''
    Requests user specified (Angular) data through HTTP call, passing derived information
    to the corresponding module of the chosen methodology and 
    redirects the computed VaR and CVaR to be outputted.
    '''
    global calculated_var
    global conditional_var

    strings = []
    weights = []

    received_data = request.get_json()
    
    methodology = received_data[0]
    tickers = received_data[1]
    
    #iterate over dictionary, strings in one array, weights in another
    for chosenTicker in tickers:
        strings.append(chosenTicker['string'])
        weights.append(chosenTicker['weight'])
    
    start_date = received_data[2]
    end_date = received_data[3]
    init_investment = received_data[4]

    if methodology == "MonteCarlo":
        print("directed to MC")

        monte_carlo_var, monte_carlo_cvar = monteCarlo.monte_carlo_calculation(strings, start_date, end_date, init_investment, weights)
        calculated_var = monte_carlo_var
        conditional_var = monte_carlo_cvar


    elif methodology == "ModelBuilding":
        print("directed to Model Building")

        model_building_var, model_building_cvar = modelBuilding.model_building_calculation(strings, start_date, end_date, init_investment, weights)
        calculated_var = model_building_var
        conditional_var = model_building_cvar

    else:
        print("directed to Historical")

        historic_var, historic_cvar = historicalSimulation.historical_calculation(strings, weights, start_date, end_date, init_investment)
        calculated_var = historic_var
        conditional_var = historic_cvar

    #CHECK
    return redirect(url_for('output_data'))
    

def get_default_values(is_multi_asset, dates):
    '''
    Fetches Quandl data corresponding to default stocks to be used when invalid tickers are entered
    '''
    if (is_multi_asset):
        flash('Using default GOOG, AAPL and GE tickers - please ensure all tickers are valid.', 'danger')
        stocks_default = ['GOOG', 'AAPL', 'GE']
        
    else:
        flash('Using default AAPL ticker - please input valid ticker.', 'danger')
        stocks_default = ['AAPL']
    
    pnl = fetch_data(stocks_default, dates)

    return pnl, stocks_default


if __name__ == '__main__':
    app.run(debug=True)
