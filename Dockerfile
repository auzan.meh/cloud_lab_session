# syntax=docker/dockerfile:1
FROM python:3.10
ENV FLASK_APP=main.py
ENV FLASK_RUN_HOST=0.0.0.0
WORKDIR /
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt


RUN echo ls
COPY . ./
EXPOSE 8080
ENTRYPOINT ["python"]
CMD ["main.py"]