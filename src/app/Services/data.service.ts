import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject('No data entered');
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  /**
   * Passes a new string into the Observable stream to be shared between components.
   * 
   * @param message - The new string message to be passed into the data stream
   */
  changeMessage(message: string) {
    this.messageSource.next(message.toUpperCase())
  }
  
}
