import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService implements OnInit {

  constructor(private http: HttpClient ) { }

  ngOnInit(): void {}

  outputUrl: string = "http://127.0.0.1:8000/account/";

  /**
   * Fetches the VaR results displayed by the Flask server from specifed URL.
   * 
   * @returns Observable containing VaR and CVaR obtained from the Flask server
   */
  displayOutput(): Observable<string> {
      return this.http.get<string>(this.outputUrl)
    }

}
