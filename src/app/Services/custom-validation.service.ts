import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomValidationService {

  constructor() { }

  /**
   * Determines whether the weights of the entered tickers sum to 1 and provides validation errors
   * if necessary.
   * 
   * @param tickers 
   * @returns Null if no tickers set or weights sum to 1, sets the relevant error to true otherwise.
   */
  weightValidator(tickers: any) {
    return (formGroup: FormGroup) => {
      const tickersControl = formGroup.controls[tickers];

      let sumWeight = 0
      const stocks = tickersControl.value
      let isSingleAsset = (stocks.length === 1)

      for (var stock of stocks) {
        sumWeight = sumWeight + stock.weight
      }

      if (!tickersControl) {
        return null;
      }

      if (tickersControl.errors && !tickersControl.errors.incorrectWeightSum) {
        return null;
      }

      if (!isSingleAsset && sumWeight !== 1) {
        tickersControl.setErrors({ incorrectWeightSum: true });
      } else {
        tickersControl.setErrors(null);
      }
      return null;
    }
  }

  /**
   * Validates whether the start date is before the end date.
   * 
   * @param startDate - The beginning of the time horizon input by the user
   * @param endDate - The end of the time horizon input by the user
   * @returns Null if no dates set or end date is after the start date, sets the relevant error to true otherwise.
   */
  dateValidator(startDate: any, endDate: any) {
    return (formGroup: FormGroup) => {
      const startDateControl = formGroup.controls[startDate];
      const endDateControl = formGroup.controls[endDate];

      if (!startDateControl || !endDateControl) {
        return null;
      }

      if (endDateControl.errors && !endDateControl.errors.datesMismatch) {
        return null;
      }

      if (startDateControl.value > endDateControl.value) {
        endDateControl.setErrors({ datesMismatch: true });
      } else {
        endDateControl.setErrors(null);
      }
      return null;
    }

  }
}
