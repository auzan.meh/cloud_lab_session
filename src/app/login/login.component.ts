import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from '../Services/data.service';
import { RestService } from '../Services/rest.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(
    private rs: RestService,
    private data: DataService,
    private route: ActivatedRoute
  ) {}

  /**
   * Initialises component with data (results) outputted by Flask via HTTP call 
   * and via query parameters, to be displayed to user.
   * 
   */
  ngOnInit(): void {}

  onSubmit(): void {}

}
