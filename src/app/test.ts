export class Portfolio {
    holdings!: {
        [key: string]: {
            price: any;
            qty: any;
        },
    }
    portfolio_value: any;
  
    getValue() {
      return JSON.stringify(this.portfolio_value)
    }

    getHoldings() {
      let holdings = Object.keys(this.holdings)

      return holdings
      //array of tickers
    }

    getPrices() {
      // let prices = JSON.stringify(Object.values(this.holdings)[0].price)
      // let holdings = JSON.stringify(Object.values(this.holdings))
      let holdings = Object.values(this.holdings)
      let prices = []

      for (var i of holdings) {
        prices.push(i.price)
      }
      
      return prices

    }

    getQty() {
      // let prices = JSON.stringify(Object.values(this.holdings)[0].price)
      // let holdings = JSON.stringify(Object.values(this.holdings))
      let holdings = Object.values(this.holdings)
      let quantity = []

      for (var i of holdings) {
        quantity.push(i.qty)
      }
      
      return quantity
    }

    calculateMakeUp() {
      // quantity * price
      let holdings = this.getHoldings()
      let prices = this.getPrices()
      let quantity = this.getQty()
      let tickerHoldings = []
      let ratios = []
      let total = 0
      for (var i in holdings) {
        tickerHoldings.push(prices[i] * quantity[i])
        total = total + (prices[i] * quantity[i])
      }

      for (var x of tickerHoldings) {
        ratios.push((x/total)*100)

      }

      return ratios


    }
    
  }

