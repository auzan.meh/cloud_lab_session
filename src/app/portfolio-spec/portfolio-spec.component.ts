import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { RestService } from '../Services/rest.service';
import { DataService } from '../Services/data.service';
import { Portfolio } from '../test';


@Component({
  selector: 'app-portfolio-spec',
  templateUrl: './portfolio-spec.component.html',
  styleUrls: ['./portfolio-spec.component.css']
})

export class PortfolioSpecComponent implements OnInit, OnDestroy {
  
  holdings: any;
  portfolio: any;
  portfolioValue: any;
  prices: any;
  quantity: any;
  ratios: any;
  message!: string;
  subscription!: Subscription;

  
  constructor(
    private rs: RestService,
    private data: DataService,
  ) { }

  /**
   * Initialises component with Reactive form for user to specify portfolio.
   * 
   */
  

  ngOnInit() {
    this.rs.displayOutput()
      .subscribe
        (
          (response) => 
          {
            this.portfolio = Object.assign(new Portfolio(), response);
            this.holdings = this.portfolio.getHoldings();
            this.prices = this.portfolio.getPrices();
            this.quantity = this.portfolio.getQty();

            this.ratios = this.portfolio.calculateMakeUp();

            this.portfolioValue = this.portfolio.getValue();
            
          },
          (error) =>
          {
            console.log("No Data Found" + error);
          }

        )
    

    this.subscription = this.data.currentMessage.subscribe(message => this.message = message)
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
