import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioSpecComponent } from './portfolio-spec.component';

describe('PortfolioSpecComponent', () => {
  let component: PortfolioSpecComponent;
  let fixture: ComponentFixture<PortfolioSpecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioSpecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioSpecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
