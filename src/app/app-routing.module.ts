import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PortfolioSpecComponent } from './portfolio-spec/portfolio-spec.component';

const routes: Routes = [
  { path: '', component: PortfolioSpecComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
